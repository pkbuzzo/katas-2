// Add 
function add (x,y) {
    return x + y;
}

console.log(add(2,3))
document.write(add(2,3))
document.write("\n")


// Multiply
function multiply(x, y) {
    let dig = 0;
    for (i = 1; i <= y; i++) {
      dig = add(dig,x);
    }
  return dig;
}

console.log(multiply(5,9))
document.write(multiply(5,9))
document.write("\n")



//Power
function power(a,b) {
    let tom = 1; 
    for (let i = 1; i <= b; i++) {
      tom = multiply(tom,a)
    }
    return tom;
}
  
console.log(power(5,5))
document.write(power(5,5))
document.write("\n")



//Factorial
function factorialize(num) {
    let result = 1;
    for (let counter = 4; counter > 0; counter--){
      result = multiply(counter,result)
    }
    return result;
  }

console.log(factorialize(4))
document.write(factorialize(4))
document.write("\n")


//Fibonacci
function fibonacci(n) {
    let [a, b] = [0, 1];
    while (n-- > 0) {
      [a, b] = [b, a + b];
    }
    return b;
}
   
console.log(fibonacci(24))
document.write(fibonacci(24))
document.write("\n")